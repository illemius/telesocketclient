"""
This is a simple echo bot using:
    - TeleSocketClient
    - python-telegram-bot library (https://github.com/python-telegram-bot/python-telegram-bot/)

It echoes any incoming text messages from webhook service.
"""

import logging

from telegram import Update
from telegram.ext import Updater, CommandHandler, MessageHandler, Filters

from TeleSocketClient import TeleSocket

# Enable logging
logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
                    level=logging.INFO)

logger = logging.getLogger(__name__)

TELEGRAM_TOKEN = 'Token for telegram bot'
TELESOCKET_TOKEN = 'Token for TeleSocket service'

# Setup connection with TeleSocket service
sock = TeleSocket()


# Define a few command handlers. These usually take the two arguments bot and
# update. Error handlers also receive the raised TelegramError object in error.
def start(bot, update):
    update.message.reply_text("""\
Hi there, I am EchoBot.
I am here to echo your kind words back to you. Just say anything nice and I'll say the exact same thing to you!\
""")


def echo(bot, update):
    update.message.reply_text(update.message.text)


def read_update(updater, update):
    upd = Update.de_json(update, updater.bot)
    updater.update_queue.put(upd)


def main():
    # Authenticate to TeleSocket service
    sock.login(TELESOCKET_TOKEN)

    # Create the EventHandler and pass it your bot's token.
    updater = Updater(TELEGRAM_TOKEN)

    # Get the dispatcher to register handlers
    dp = updater.dispatcher

    # on different commands - answer in Telegram
    dp.add_handler(CommandHandler("start", start))
    dp.add_handler(CommandHandler("help", start))

    # on noncommand i.e message - echo the message on Telegram
    dp.add_handler(MessageHandler(Filters.text, echo))

    # Start the Bot
    updater.job_queue.start()
    updater._init_thread(updater.dispatcher.start, "dispatcher")

    # Set updater status
    updater.running = True

    # Add updates handler
    sock.add_telegram_handler(lambda update: read_update(updater, update))

    # Get webhook URL for bot
    webhook = sock.set_webhook(updater.bot.username)

    # Setup webhook
    updater.bot.set_webhook(webhook_url=webhook.url)

    # Run the bot until you press Ctrl-C or the process receives SIGINT,
    # SIGTERM or SIGABRT. This should be used most of the time, since
    # start_polling() is non-blocking and will stop the bot gracefully.
    updater.idle()


if __name__ == '__main__':
    main()
