"""
This is a simple echo bot using:
    - TeleSocketClient
    - pyTelegramBotAPI library (https://github.com/eternnoir/pyTelegramBotAPI)

It echoes any incoming text messages from webhook service.
"""

import telebot

from TeleSocketClient import TeleSocket

TELEGRAM_TOKEN = 'Token for telegram bot'
TELESOCKET_TOKEN = 'Token for TeleSocket service'

# Setup connection with TeleSocket service
sock = TeleSocket(daemonic=False)

# Create bot instance
bot = telebot.TeleBot(TELEGRAM_TOKEN)


def handle_updates(update):
    """
    Updates serializer
    :param update: dict
    :return:
    """
    # Serialize
    update = telebot.types.Update.de_json(update)
    # Notify bot for new update
    bot.process_new_updates([update])


# Handle '/start' and '/help'
@bot.message_handler(commands=['help', 'start'])
def send_welcome(message):
    bot.reply_to(message, """\
Hi there, I am EchoBot.
I am here to echo your kind words back to you. Just say anything nice and I'll say the exact same thing to you!\
""")


# You can get ping to TeleSocket service
@bot.message_handler(commands=['ping'])
def send_ping(message):
    ping = sock.ping()
    bot.reply_to(message, f"Pong: {ping} ms.")


# Handle all other messages with content_type 'text' (content_types defaults to ['text'])
@bot.message_handler(func=lambda message: True)
def echo_message(message):
    bot.reply_to(message, message.text)


if __name__ == '__main__':
    # Authenticate to TeleSocket service
    sock.login(TELESOCKET_TOKEN)

    # Register telegram handler // You updates serializer
    sock.add_telegram_handler(handle_updates)

    # Get webhook URL for bot
    webhook = sock.set_webhook(bot.get_me().username)

    # Setup webhook
    bot.set_webhook(url=webhook.url)
