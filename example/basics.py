from TeleSocketClient import TeleSocket

# Create connection
sock = TeleSocket(daemonic=False)

# Check ping (client -> server -> client)
print('Ping', sock.ping(), 'ms.')

# Try login
user = sock.login('YOU TOKEN HERE')

# Get username
print('Logged in as @' + user.username)
