from TeleSocketClient import TeleSocket

ADDRESS = ('localhost', 39620)
token = 'Token for TeleSocket service'

sock = TeleSocket()

if not token:
    user = sock.register('illemius', '8c970e76a655f3a8')
    print('TOKEN', user.token)
    token = user.token

user = sock.login(token)
print(str(user))
