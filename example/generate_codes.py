from TeleSocketClient.manager import TeleSocket

TOKEN = 'Token for TeleSocket service'
COUNT = 5

sock = TeleSocket()
sock.login(TOKEN)

for code in sock.generate_codes(COUNT).get('codes', []):
    print(code)
